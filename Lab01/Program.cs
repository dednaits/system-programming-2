﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Lab01_ConsoleApp
{
    class Program
    {
        public static int[] createIntRandomArray(int size, int from, int to)
        {
            int[] data = new int[size];
            var random = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < size; ++i)
            {
                data[i] = random.Next(from, to);
            }
            return data;
        }
        static void Minimum(object ara)
        {
            int[] arr = (int[])ara;
            int result = arr[0];
            for (int i = 0; i < arr.Length; i++)
            {
                if (result > arr[i])
                {
                    result = arr[i];
                }
            }
            Console.WriteLine("\nMinimum:{0} ", result);
        }
        static void Maximum(object ara)
        {
            int[] arr = (int[])ara;
            int result = arr[0];
            for (int i = 0; i < arr.Length; i++)
            {
                if (result < arr[i])
                {
                    result = arr[i];
                }
            }
            Console.WriteLine("Maximum:{0} ", result);
        }
        static void Averages(object ara)
        {
            int[] arr = (int[])ara;
            int result = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                result += arr[i];
            }
            result = result / 10;
            Console.WriteLine("Averages:{0} ", result);
        }

        static void SortArr(int[] arr)
        {
            Console.WriteLine("Sorting arrays");
            Array.Sort(arr);
            for (int i = 0; i < arr.Length; i++)
                Console.Write(arr[i] + " ");

        }
        static void Main(string[] args)
        {
            Random rmd = new Random();
            int a = rmd.Next(0, 100);
            int[] arr = createIntRandomArray (a, 100, 1000);
            
            SortArr(arr);

            Thread t1 = new Thread(new ParameterizedThreadStart(Minimum));
            t1.Start(arr);
            Thread.Sleep(500);
            
            Thread t2 = new Thread(new ParameterizedThreadStart(Averages));
            t2.Start(arr);
            Thread.Sleep(500);

            Thread t3 = new Thread(new ParameterizedThreadStart(Maximum));
            t3.Start(arr);
            Thread.Sleep(500);

            Console.ReadKey();
        }
    }
}
